<!--
SPDX-FileCopyrightText: 2022 - 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: MIT
-->

# GPU Tools

Un utilitaire d'autoconfiguration des frameworks **Tensorflow**, et **PyTorch**.

## Pourquoi cet outil ?
Cet outil a pour but de simplifier l'utilisation de certaines bibliothèques de deep learning, dans
le cadre de GPUs partagés à plusieurs utilisateurs.

GPU Tools permet simplement:
 * De choisir automatiquement un placement approprié pour les tâches GPU.
 * De limiter la mémoire GPU prise lors de l'utilisation des bibliothèques de deep learning.


## Utilisation simple
### Avec tensorflow
```python
from gpu_tools import get_tensorflow_autoconfig
config = get_tensorflow_autoconfig()
config.initialize()
```

### Avec pytorch
```python
from gpu_tools import get_pytorch_autoconfig
config = get_pytorch_autoconfig()
config.initialize()
...
torch_device = config.device
```

## Paramétrage
### Réglage de la quantité de mémoire utilisable
Par défaut GPU Tools autorise un maximum de 12 Go de mémoire GPU par application.
Il est possible de changer cette valeur en configurant l'attribut `memory_limit` d'un configuration,
**avant l'appel à** `initialize`.

```python
config = get_tensorflow_autoconfig()  # ou get_pytorch_autconfig()
config.memory_limit = 24 * 1024  # Demande 24Go de mémoire utilisable
config.initialize()
```

### Réglage de la stratégie d'allocation
Par défaut GPU Tools alloue les ressources de calcul sur le GPU avec le plus de VRAM.
Il est possible de changer de stratégie d'allocation via l'attribut `strategy`.
```python
config = get_tensorflow_autoconfig()  # ou get_pytorch_autconfig()
config.strategy = "free"  # Demande un GPU complètement libre
config.initialize()
```

Les stratégies possibles sont les suivantes :
- `"memory"` (défaut) : Alloue sur le GPU avec le plus de VRAM.
- `"load"` : Alloue sur le GPU le moins chargé en calcul.
- `"free"` : Alloue sur un GPU libre.
