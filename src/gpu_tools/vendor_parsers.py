# SPDX-FileCopyrightText: 2022 - 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

# pylint: disable=arguments-renamed,no-value-for-parameter

import xml.etree.ElementTree as XMLTree
from collections import defaultdict
from collections.abc import Sequence, Mapping, Callable
from dataclasses import dataclass, field
from abc import ABC, abstractmethod
from subprocess import run
from functools import wraps
from typing import cast, Optional, Self, TypeVar, TypedDict

from .types import GpuPciId

T = TypeVar("T")


class MemoryStatsDict(TypedDict):
    total: int
    used: int
    free: int


class ClockStatsDict(TypedDict):
    graphics: int
    sm: int
    memory: int
    video: int


class AllStatsDict(TypedDict):
    gpu_name: Optional[str]
    load: Optional[float]
    memory: Optional[MemoryStatsDict]
    temperature: Optional[int]
    power: Optional[float]
    clock: Optional[ClockStatsDict]
    fan_speed: Optional[int]


def get_element(
    xml_root: XMLTree.Element,
    selector: str,
    cast_type: Callable[[str], T],
    suffix: Optional[str] = None,
) -> Optional[T]:
    text = xml_root.findtext(selector)
    if not text or text == "N/A":
        return None

    if suffix:
        text = text.removesuffix(suffix)
    return cast_type(text)


class VendorParser(ABC):
    @abstractmethod
    def get_gpu_ids(self) -> Sequence[GpuPciId]:
        pass

    @abstractmethod
    def get_gpu_names(self) -> Mapping[GpuPciId, Optional[str]]:
        pass

    @abstractmethod
    def get_current_loads(self) -> Mapping[GpuPciId, Optional[float]]:
        pass

    @abstractmethod
    def get_memory_stats(self) -> Mapping[GpuPciId, Optional[MemoryStatsDict]]:
        pass

    @abstractmethod
    def get_temperature(self) -> Mapping[GpuPciId, Optional[int]]:
        pass

    @abstractmethod
    def get_power(self) -> Mapping[GpuPciId, Optional[float]]:
        pass

    @abstractmethod
    def get_clock_stats(self) -> Mapping[GpuPciId, Optional[ClockStatsDict]]:
        pass

    @abstractmethod
    def get_fan_speed(self) -> Mapping[GpuPciId, Optional[int]]:
        pass

    @abstractmethod
    def get_all_stats(self) -> Mapping[GpuPciId, AllStatsDict]:
        pass


@dataclass(frozen=True)
class NvidiaParser(VendorParser):
    gpus_info: Mapping[GpuPciId, XMLTree.Element] = field(repr=False)

    @staticmethod
    def _get_nvidia_smi_info() -> XMLTree.Element:
        output = run(["nvidia-smi", "--query", "--xml-format"], check=True, capture_output=True)
        return XMLTree.fromstring(output.stdout)

    @staticmethod
    def _iter_on_gpus(smi_info: XMLTree.Element) -> Mapping[GpuPciId, XMLTree.Element]:
        return {GpuPciId(gpu.attrib["id"]): gpu for gpu in smi_info.findall("gpu")}

    @classmethod
    def from_cli(cls) -> Self:
        smi_info = cls._get_nvidia_smi_info()
        return cls(cls._iter_on_gpus(smi_info))

    @staticmethod
    def wrap_on_gpuid(
        func: Callable[[XMLTree.Element], T]
    ) -> Callable[..., Mapping[GpuPciId, T]]:  # cannot type with [Self] here...
        @wraps(func)
        def wrapper(self: Self) -> Mapping[GpuPciId, T]:
            return {gpu_id: func(gpu) for gpu_id, gpu in self.gpus_info.items()}

        return wrapper

    def get_gpu_ids(self) -> Sequence[GpuPciId]:
        return list(self.gpus_info)

    @wrap_on_gpuid
    @staticmethod
    def get_gpu_names(gpu: XMLTree.Element) -> Optional[str]:
        return gpu.findtext("product_name")

    @wrap_on_gpuid
    @staticmethod
    def get_current_loads(gpu: XMLTree.Element) -> Optional[float]:
        return get_element(gpu, "./utilization/gpu_util", float, suffix="%")

    @wrap_on_gpuid
    @staticmethod
    def get_memory_stats(gpu: XMLTree.Element) -> Optional[MemoryStatsDict]:
        memory_usage = gpu.find("fb_memory_usage")
        if memory_usage:
            return {  # type: ignore[return-value]
                key: int(cast(str, memory_usage.findtext(key)).removesuffix("MiB")) for key in ("total", "used", "free")
            }
        else:
            return None

    @wrap_on_gpuid
    @staticmethod
    def get_temperature(gpu: XMLTree.Element) -> Optional[int]:
        return get_element(gpu, "./temperature/gpu_temp", int, suffix="C")

    @wrap_on_gpuid
    @staticmethod
    def get_power(gpu: XMLTree.Element) -> Optional[float]:
        return get_element(gpu, "./power_readings/power_draw", float, suffix="W")

    @wrap_on_gpuid
    @staticmethod
    def get_clock_stats(gpu: XMLTree.Element) -> Optional[ClockStatsDict]:
        clock_usage: XMLTree.Element = gpu.find("clocks")  # type: ignore

        def find_value(xml_key: str) -> int:
            prop: str = clock_usage.findtext(xml_key)  # type: ignore
            assert prop, xml_key
            return int(prop.removesuffix("MHz"))

        if not len(clock_usage):  # deprecated __bool__ usage
            return None
        return {
            "graphics": find_value("graphics_clock"),
            "memory": find_value("mem_clock"),
            "sm": find_value("sm_clock"),
            "video": find_value("video_clock"),
        }

    @wrap_on_gpuid
    @staticmethod
    def get_fan_speed(gpu: XMLTree.Element) -> Optional[int]:
        return get_element(gpu, "./fan_speed", int, suffix="%")

    def get_all_stats(self) -> Mapping[GpuPciId, AllStatsDict]:
        all_dicts = {
            "gpu_name": self.get_gpu_names(),
            "load": self.get_current_loads(),
            "memory": self.get_memory_stats(),
            "temperature": self.get_temperature(),
            "power": self.get_power(),
            "clock": self.get_clock_stats(),
            "fan_speed": self.get_fan_speed(),
        }
        dict_by_gpu_id: dict[GpuPciId, AllStatsDict] = defaultdict(dict)  # type: ignore
        for stat_name, stat_dict in all_dicts.items():
            for gpu_id, stat_value in stat_dict.items():
                dict_by_gpu_id[gpu_id][stat_name] = stat_value  # type: ignore[literal-required]
        return dict_by_gpu_id
