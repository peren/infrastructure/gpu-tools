# SPDX-FileCopyrightText: 2022 - 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import logging
from os import environ
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from collections.abc import Mapping, Callable
from typing import Optional, TypeVar, Final

from .types import GpuPciId, GpuCudaId, AllocationStrategies
from .cuda import get_devices_pci_id
from .exceptions import ValidationError, UninitializedConfigError, StrategyApplicationError

from .vendor_parsers import VendorParser


T = TypeVar("T")


def _are_devices_overridden() -> bool:
    return environ.get("CUDA_VISIBLE_DEVICES") == "-1" and environ.get(
        "GPUTOOLS_OVERRIDE_EMPTY_DEVICES", "0"
    ).lower() in ["true", "1"]


@dataclass
class FrameworkConfigurator(ABC):

    vendor_parser_factory: Final[Callable[[], VendorParser]]  # type: ignore[misc]

    strategy: int | AllocationStrategies = AllocationStrategies.memory
    """GPU_id or allocation strategy from enum."""
    memory_limit: Optional[int] = 12 * 1024
    """Memory limit (in MiB)"""

    initialized: bool = field(default=False, init=False)

    def __post_init__(self) -> None:
        if not (isinstance(self.strategy, int) and self.strategy >= 0) and not isinstance(
            self.strategy, AllocationStrategies
        ):
            raise ValidationError(
                f"strategy must be a GPU_id:int >= 0, or an allocation strategy among {list(AllocationStrategies)}"
            )
        if self.memory_limit is not None and self.memory_limit <= 0:
            raise ValidationError("memory_limit should be > 0 (absolute in MiB)")

    @abstractmethod
    def _pin_gpu(self, gpu_id: GpuCudaId, vendor_parser: VendorParser) -> None:
        pass

    def find_best_gpu(self) -> tuple[GpuCudaId, VendorParser]:
        gpu_pool = {pci_id: cuda_id for cuda_id, pci_id in get_devices_pci_id().items()}
        vendor_parser = self.vendor_parser_factory()

        if isinstance(self.strategy, int):
            pci_ids = vendor_parser.get_gpu_ids()
            if self.strategy >= len(pci_ids):
                raise ValidationError(f"Asked for GPU #{self.strategy} (0-indexed), but only {len(pci_ids)} GPUs found")

            pci_id = pci_ids[self.strategy]
            cuda_id = gpu_pool.get(pci_id)

            if cuda_id is None:
                raise ValidationError(
                    f"GPU #{self.strategy} with {pci_id=} exists"
                    ", but is probably not allowed in 'CUDA_VISIBLE_DEVICES'"
                )

            return cuda_id, vendor_parser

        def remap(pci_dict: Mapping[GpuPciId, Optional[T]]) -> Mapping[GpuCudaId, T]:
            remapped = {
                gpu_pool[pci_id]: value
                for pci_id, value in pci_dict.items()
                if pci_id in gpu_pool and value is not None  # TODO? warn for unknown values?
            }
            if not remapped:
                raise StrategyApplicationError(f"No GPU with valid metadata for {self.strategy}")
            return remapped

        match self.strategy:
            case AllocationStrategies.memory:
                memory_dict = remap(vendor_parser.get_memory_stats())
                return max(memory_dict, key=lambda x: memory_dict[x]["free"]), vendor_parser
            case AllocationStrategies.load:
                load_dict = remap(vendor_parser.get_current_loads())
                return min(load_dict, key=load_dict.__getitem__), vendor_parser
            case AllocationStrategies.free:
                memory_dict = remap(vendor_parser.get_memory_stats())
                for cuda_id, memory in memory_dict.items():
                    if memory["used"] == 0:
                        return cuda_id, vendor_parser
                raise StrategyApplicationError(f"No free GPU found: GPUs memory (MiB) = {memory_dict}")
            case _:
                raise ValidationError(f"Invalid strategy {self.strategy}")

    def initialize(self, *, ignore_visible_devices: Optional[bool] = None) -> None:
        ignore_visible_devices = _are_devices_overridden() if ignore_visible_devices is None else ignore_visible_devices
        if self.initialized:
            raise ValidationError("GPU was already initialized")
        if ignore_visible_devices and "CUDA_VISIBLE_DEVICES" in environ:
            del environ["CUDA_VISIBLE_DEVICES"]
        gpu_id, vendor_parser = self.find_best_gpu()
        self._pin_gpu(gpu_id, vendor_parser)
        self.initialized = True


class CpuFallbackDummyConfigurator(FrameworkConfigurator):
    def find_best_gpu(self) -> tuple[GpuCudaId, VendorParser]:
        raise NotImplementedError

    def _pin_gpu(self, gpu_id: GpuCudaId, vendor_parser: VendorParser) -> None:
        raise NotImplementedError

    def initialize(self, *, ignore_visible_devices: bool | None = None) -> None:
        logging.warning("%s is an early implementation that does almost nothing", self.__class__.__name__)
        self.initialized = True


@dataclass
class TensorflowConfigurator(FrameworkConfigurator):
    def _set_devices_properties(
        self,
        devices: list["tensorflow.config.PhysicalDevice"],  # type: ignore[name-defined] # noqa: F821
    ) -> None:
        import tensorflow as tf

        for compute_device in devices:
            if compute_device.device_type == "GPU":
                tf.config.experimental.set_memory_growth(compute_device, True)
                if self.memory_limit:
                    tf.config.set_logical_device_configuration(
                        compute_device, [tf.config.LogicalDeviceConfiguration(memory_limit=self.memory_limit)]
                    )
        tf.config.set_visible_devices(devices)
        tf.config.list_logical_devices()  # This produces a side-effect initializing the devices


class TensorflowGpuConfigurator(TensorflowConfigurator):
    def _pin_gpu(self, gpu_id: GpuCudaId, vendor_parser: VendorParser) -> None:
        import tensorflow as tf

        gpus = tf.config.list_physical_devices("GPU")
        self._set_devices_properties([gpus[gpu_id]])


class TensorflowCpuConfigurator(TensorflowConfigurator, CpuFallbackDummyConfigurator):
    def initialize(self, *, ignore_visible_devices: bool | None = None) -> None:
        import tensorflow as tf

        cpus = tf.config.list_physical_devices("CPU")
        self._set_devices_properties(cpus)
        self.initialized = True


@dataclass
class PytorchConfigurator(FrameworkConfigurator):
    _device: Optional["torch.device"] = field(default=None, init=False)  # type: ignore # noqa: F821

    @property
    def device(self) -> "torch.device":  # type: ignore # noqa: F821
        if self._device is None:
            raise UninitializedConfigError("initialize() must be called before a device is available")
        return self._device


class PytorchCpuConfigurator(PytorchConfigurator, CpuFallbackDummyConfigurator):
    def initialize(self, *, ignore_visible_devices: bool | None = None) -> None:
        import torch

        self._device = torch.device("cpu")
        return super().initialize(ignore_visible_devices=ignore_visible_devices)


class PytorchGpuConfigurator(PytorchConfigurator, FrameworkConfigurator):
    def _pin_gpu(self, gpu_id: GpuCudaId, vendor_parser: VendorParser) -> None:
        import torch
        import torch.cuda as pt_cuda

        pt_cuda.set_device(gpu_id)
        self._device = torch.device(f"cuda:{gpu_id}")
        if self.memory_limit:
            memory_dict = vendor_parser.get_memory_stats()[get_devices_pci_id()[gpu_id]]
            if not memory_dict:
                raise StrategyApplicationError(f"Unable to retrieve memory metadata for {gpu_id=}")
            max_memory = memory_dict["total"]
            memory_ratio = min(self.memory_limit / max_memory, 1.0)
            pt_cuda.set_per_process_memory_fraction(memory_ratio, gpu_id)
        pt_cuda.init()  # type: ignore[no-untyped-call]
