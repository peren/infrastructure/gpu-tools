# SPDX-FileCopyrightText: 2022 - 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT


class GPUToolsException(Exception):
    pass


class ValidationError(GPUToolsException, ValueError):
    pass


class UninitializedConfigError(GPUToolsException):
    pass


class StrategyApplicationError(GPUToolsException, RuntimeError):
    pass


class GPUToolsRuntimeError(GPUToolsException, RuntimeError):
    pass
