# SPDX-FileCopyrightText: 2022 - 2023 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import importlib.metadata
import logging
from subprocess import CalledProcessError, run
from typing import Any

from .exceptions import GPUToolsRuntimeError
from .framework_configurators import (
    PytorchConfigurator,
    PytorchCpuConfigurator,
    PytorchGpuConfigurator,
    TensorflowConfigurator,
    TensorflowCpuConfigurator,
    TensorflowGpuConfigurator,
)
from .types import AllocationStrategies
from .vendor_parsers import VendorParser, NvidiaParser

__version__ = importlib.metadata.version(__name__)

__all__ = [
    "AllocationStrategies",
    "PytorchConfigurator",
    "TensorflowConfigurator",
    "NvidiaParser",
    "__version__",
]


_FALLBACK_ADVERTISEMENT = "Use cpu_fallback=True to fallback to CPU"


def _check_nvidia_smi() -> None:
    try:
        run("nvidia-smi", check=True, capture_output=True)
    except FileNotFoundError as e:
        raise GPUToolsRuntimeError("Cannot use autoconfiguration: command nvidia-smi not found.") from e
    except CalledProcessError as e:
        raise GPUToolsRuntimeError(
            "Cannot use autoconfiguration: command nvidia-smi returned non-zero error code."
        ) from e


def get_tensorflow_autoconfig(cpu_fallback: bool = False, **kws: Any) -> TensorflowConfigurator:
    try:
        _check_nvidia_smi()
        return TensorflowGpuConfigurator(NvidiaParser.from_cli, **kws)
    except GPUToolsRuntimeError as e:
        if cpu_fallback:
            logging.warning("Using CPU fallback.")
            return TensorflowCpuConfigurator(VendorParser, **kws)
        else:
            e.add_note(_FALLBACK_ADVERTISEMENT)
            raise e


def get_pytorch_autoconfig(cpu_fallback: bool = False, **kws: Any) -> PytorchConfigurator:
    try:
        _check_nvidia_smi()
        return PytorchGpuConfigurator(NvidiaParser.from_cli, **kws)
    except GPUToolsRuntimeError as e:
        if cpu_fallback:
            logging.warning("Using CPU fallback.")
            return PytorchCpuConfigurator(VendorParser, **kws)
        else:
            e.add_note(_FALLBACK_ADVERTISEMENT)
            raise e
